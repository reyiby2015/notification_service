import requests
import os
from loguru import logger
from celery import shared_task
from dotenv import load_dotenv
from mailing.models import Message
import datetime
import pytz


# logger.remove(0)

# Метод remove() вызывается первым, чтобы удалить конфигурацию для обработчика по умолчанию
# (идентификатор которого равен 0).


load_dotenv()
URL = os.getenv('URL')
TOKEN = os.getenv('TOKEN')
HEADER = {
    'Authorization': f'Bearer {TOKEN}',
    'Content-Type': 'application/json'
}


@shared_task(
    bind=True, autoretry_for=(Exception,), retry_backoff=True,
    retry_jitter=True, retry_kwargs={'max_retries': 100}
    )
def start_mailing(self, data, message_id):
    """Function for start mailing tasks"""

    message = Message.objects.get(pk=message_id)
    timezone = pytz.timezone(message.client.time_zone)
    now_with_tz_cli = timezone.localize(datetime.datetime.now())

    logger.add(f"logs/debug_{message.mailing}.log", format="{time:MMMM D YYYY > HH:mm:ss} | {level} | {message}", rotation="1 MB")

    if message.mailing.launch_time < now_with_tz_cli < message.mailing.stop_time:
        response = requests.post(url=URL + str(data['id']), headers=HEADER, json=data)
        logger.debug(f"Start mailing for the {message.client}")
        if response.status_code == 200:
            message.status = True
            message.save()
            logger.success(f"The {message} was sent successfully. {response.text}")
        elif response.status_code == 401:
            logger.error(f"Token contains an invalid number of segments. {response.text}")
        elif response.status_code == 400:
            logger.error(f"Request error. {response.text}")

    elif message.mailing.launch_time > now_with_tz_cli:
        logger.info(f"Mailing for the {message.client} will be started at {message.mailing.launch_time}")
        return self.retry(eta=message.mailing.launch_time)
    else:
        logger.error(f"The mailing time is over for the {message.client}")
        # revoke()

