from rest_framework.response import Response
from rest_framework import viewsets

from .models import Client, Message, Mailing
from .serializers import ClientSerializer, MessageSerializer, MailingSerializer


class ClientViewset(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class MessageViewset(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer


class MailingViewset(viewsets.ModelViewSet):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer