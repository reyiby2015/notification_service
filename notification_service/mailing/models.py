import pytz
from django.db import models
from django.core.validators import RegexValidator


class Client(models.Model):
    """Client's model"""
    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))
    phoneNumberRegex = RegexValidator(regex=r"^\+?7\d{10}$")
    phone_number = models.CharField(max_length=11,
                                    validators=[phoneNumberRegex],
                                    db_index=True,
                                    unique=True,
                                    verbose_name='Phone number')
    mobile_operator_code = models.CharField(
        max_length=3, help_text="It will be filled in automatically",
        blank=True, verbose_name='Mobile operator code')
    tag = models.CharField(max_length=100, unique=True, verbose_name='Tag')
    time_zone = models.CharField(max_length=32,
                                 choices=TIMEZONES,
                                 default='Europe/Moscow',
                                 verbose_name='Time Zone')

    def save(self, *args, **kwargs):
        self.mobile_operator_code = str(self.phone_number)[1:4]
        return super(Client, self).save(*args, **kwargs)
    

    def __str__(self):
        return f"{self.phone_number} client"

    class Meta:
        verbose_name = 'Client'
        verbose_name_plural = 'Clients'


class Mailing(models.Model):
    """Mailing model"""
    launch_time = models.DateTimeField(verbose_name='Mailing launch time')
    text = models.TextField(max_length=300, verbose_name="Message text")
    stop_time = models.DateTimeField(verbose_name='Mailing stop time')
    moc_client = models.CharField(max_length=3, verbose_name="Filter clients by mobile opertator code")
    tag_client = models.CharField(max_length=100, blank=True, verbose_name="Filter clients by tag")


    def __str__(self):
        return f"Mailing {self.pk} id from {self.launch_time.strftime('%b %d %Y > %H:%M:%S')}"

    class Meta:
        verbose_name = 'Mailing'
        verbose_name_plural = 'Mailings'



class Message(models.Model):
    sending_time = models.DateTimeField(auto_now_add=True,
                                        verbose_name='Message sending time')
    status = models.BooleanField(verbose_name='Sent', default=None)
    mailing = models.ForeignKey(Mailing,
                                on_delete=models.CASCADE,
                                related_name='messages')
    client = models.ForeignKey(Client,
                               on_delete=models.CASCADE,
                               related_name='messages')

    def __str__(self):
        return f"Message from {self.mailing} for {self.client}"

    class Meta:
        verbose_name = 'Message'
        verbose_name_plural = 'Messages'
