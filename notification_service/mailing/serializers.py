from rest_framework import serializers
from .models import Client, Message, Mailing


class ClientSerializer(serializers.ModelSerializer):

    class Meta:
        model = Client
        fields = "__all__"


class MessageSerializer(serializers.ModelSerializer):
    client = serializers.SlugRelatedField(slug_field="phone_number",
                                          read_only=True)
    mailing = serializers.SlugRelatedField(slug_field="text", read_only=True)

    class Meta:
        model = Message
        fields = "__all__"


class MailingSerializer(serializers.ModelSerializer):

    client = serializers.SlugRelatedField(slug_field="phone_number",
                                          read_only=True,
                                          many=True)
    client = ClientSerializer

    class Meta:
        model = Mailing
        fields = "__all__"