from django.contrib import admin
from .models import Client, Message, Mailing


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ('phone_number', 'mobile_operator_code', 'tag', 'time_zone')
    prepopulated_fields = {'mobile_operator_code': ('phone_number',)}
    fields = ('phone_number', 'mobile_operator_code', 'tag', 'time_zone')


@admin.register(Mailing)
class MailingAdmin(admin.ModelAdmin):
    save_on_top = True
    list_display = (
        'launch_time',
        'text'
    )
    # list_filter = ('client__mobile_operator_code', 'client__phone_number')
    fields = ('launch_time', 'text', 'moc_client', 'tag_client', 'stop_time')



@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('mailing', 'client', 'status')
    readonly_fields = ('sending_time', 'mailing', 'client', 'status')
    fields = ('sending_time', 'mailing', 'client', 'status')
