from django.db.models.signals import post_save
from django.dispatch import receiver
from mailing.models import Message, Mailing, Client
from .tasks import start_mailing


@receiver(post_save, sender=Mailing)
def create_mailing(sender, instance, **kwargs):
    mailing = instance
    clients = Client.objects.filter(mobile_operator_code=mailing.moc_client)
    for client in clients:
        message = Message.objects.create(status=False, client=client, mailing=mailing)
        phone = client.phone_number
        text = mailing.text
        data = {
            "id": message.pk,
            "phone": phone,
            "text": str(text)
        }
        start_mailing.apply_async(args=[data, message.pk], retry_policy={
            'max_retries': None,
            'interval_start': 1,
            'interval_step': 0.2,
            'interval_max': 0.2,
        })
