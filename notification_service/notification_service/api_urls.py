from rest_framework import routers
from mailing.api_views import ClientViewset, MessageViewset, MailingViewset

router = routers.DefaultRouter()
router.register(r'clients', ClientViewset)
router.register(r'messages', MessageViewset)
router.register(r'mailing', MailingViewset)
